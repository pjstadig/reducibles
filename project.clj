(defproject reducibles "0.3.1-SNAPSHOT"
  :description "Reducibles are cool!"
  :url "http://gitlab.com/pjstadig/reducibles/"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :deploy-repositories [["releases" :clojars]]
  :dependencies [[org.clojure/clojure "1.8.0"]]
  :profiles {:dev {:dependencies [[com.cognitect/transit-clj "0.8.313"]]}})
