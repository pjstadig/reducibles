# Change Log
All notable changes to this project will be documented in this file. This change log follows the conventions of [keepachangelog.com](http://keepachangelog.com/).

## [Unreleased]
### Changed
-

### Fixed
-

## [0.3.0]
### Changed
- Ignore Unicode byte order mark at beginning of CSV file.

### Fixed
- Actually default quote character to “.

## [0.2.0]
### Changed
- Changed namespace from pjstadig.reducibles -> reducibles.core

## 0.1.0
### Added
- Initial release.

[Unreleased]: https://gitlab.com/pjstadig/reducibles/compare/0.3.0...master
[0.3.0]: https://gitlab.com/pjstadig/reducibles/compare/0.2.0...0.3.0
[0.2.0]: https://gitlab.com/pjstadig/reducibles/compare/0.1.0...0.2.0
