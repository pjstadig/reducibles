# reducibles

A library of reducibles that decode CSV, edn, Clojure data, and transit.

Each reducible will manage its resources closing any stream that it opened.

The true value of this library is creating a source for transducers that manages its own stream.

## Usage

Add a dependency to your project:

    [reducibles "0.3.0"]

There are two core reducibles `decoding-reducible` and `binding-reducible`. When reduced, `decoding-reducible` will open a stream, decode the stream using the supplied `decode` function, then close the stream when the reduction is complete or if an error occurs.

`binding-reducible` will push a binding, perform a reduction, then pop the binding when the reduction is complete or if an error occurs.

Built on top of these are `csv-reducible`, `edn-reducible`, `clojure-reducible`, and `transit-reducible` that each decode their respective data from a streamable object (i.e. anything that works with `clojure.java.io/input-stream`).

Each of these takes a streamable as its first argument, and an options map as its second.  See the docstrings of each function for more details on options.

Example of decoding CSV data:

Input file:

    Model,Price,Description
    Foo,15,A foo'ing widget
    Bar,17,Premium barabulator

Code:

    (require '[reducibles.core :as r])
    (require '[clojure.java.io :as io])
    (into []
          (r/csv-reducible (io/file "inputfile.csv") 
                           {:header {"Model" :model, "Price" :price}}))
    => [{:model "Foo", :price "15"} {:model "Bar", :price "17"}]

Since it opens a fresh stream each time, the reducible can be reused:

    (def csv
         (eduction (map (fn [m] (update m :price #(Long/parseLong %))))
                   (r/csv-reducible (io/file "inputfile.csv")
                                    {:header {"Model" :model, "Price" :price}})))

Using transducers:

    (into [] (filter (comp #(> % 15) :price)) csv)
    => [{:model "Bar", :price 17}]

The transit library is not a dependency of `reducibles` because it is required at runtime, as needed.

## License

Copyright © Paul Stadig.

Distributed under the Eclipse Public License either version 1.0 or (at your option) any later version.
