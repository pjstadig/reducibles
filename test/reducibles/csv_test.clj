(ns reducibles.csv-test
  (:require
   [clojure.test :refer :all]
   [reducibles.csv :refer :all])
  (:import
   (java.io PushbackReader StringReader)))

(defn parse
  ([s]
   (parse s (long \") (long \,)))
  ([s quote ^long separator]
   (with-open [stream (PushbackReader. (StringReader. s))]
     (->> (repeatedly #(parse-record stream quote separator))
          (take-while some?)
          doall))))

(deftest t-parsing
  (testing "line endings"
    (is (= [["a" "b" "c"] ["1" "2" "3"]] (parse "a,b,c\n1,2,3\n")))
    (is (= [["a" "b" "c"] ["1" "2" "3"]] (parse "a,b,c\r1,2,3\r")))
    (is (= [["a" "b" "c"] ["1" "2" "3"]] (parse "a,b,c\r\n1,2,3\r\n")))
    (is (= [["a" "b" "c"] ["1" "2" "3"]] (parse "a,b,c\n1,2,3"))))
  (testing "empty fields"
    (is (= [["a" ""]] (parse "a,")))
    (is (= [["" "a"]] (parse ",a")))
    (is (= [["a" "" "b"]] (parse "a,,b"))))
  (testing "quoting"
    (is (= [["a\""]] (parse "a\""))
        "quote must be at beginning of field")
    (is (= [[""]] (parse "\"\"")))
    (is (= [["" "a"]] (parse "\"\",a")))
    (is (= [["a" ""]] (parse "a,\"\"")))
    (is (= [["a" "" "b"]] (parse "a,\"\",b")))
    (is (= [[",\"\n" "b"]] (parse "\",\"\"\n\",b"))
        "quoted field can include separators, quotes, and newlines")
    (is (= [["a" "b,c" "d"]] (parse "a,|b,c|,d" (long \|) (long \,))))
    (is (= [["a" "\"b" "c\"" "d"]] (parse "a,\"b,c\",d" nil (long \,)))
        "should disable quoted fields")
    (is (thrown? Exception (parse "a,\"b\"c"))))
  (testing "separator"
    (is (= [["a" "b|c" "d"]] (parse "a|\"b|c\"|d" (long \") (long \|)))))
  (testing "unterminated quote"
    (is (thrown? Exception (parse "\"")))))
