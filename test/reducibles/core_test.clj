(ns reducibles.core-test
  (:require
   [clojure.edn :as edn]
   [clojure.java.io :as io]
   [clojure.test :refer :all]
   [cognitect.transit :as transit]
   [reducibles.core :refer :all])
  (:import
   (java.io ByteArrayOutputStream PushbackReader StringReader)))

(defn input-stream
  [streamable closed?]
  (let [in (io/input-stream streamable)]
    (reset! closed? false)
    (proxy [java.io.InputStream] []
      (available []
        (.available in))
      (close []
        (reset! closed? true)
        (.close in))
      (mark [read-limit]
        (.mark in read-limit))
      (markSupported []
        (.markSupported in))
      (read
        ([]
         (.read in))
        ([bytes]
         (.read in bytes))
        ([bytes off len]
         (.read in bytes off len)))
      (reset []
        (.reset in))
      (skip [n]
        (.skip in n)))))

(deftest t-decoding-reducible
  (let [closed? (atom false)]
    (letfn
        [(open [s]
           #(PushbackReader. (io/reader (input-stream (.getBytes s) closed?))))
         (decode [stream eof]
           (edn/read {:eof eof} stream))]
      (testing "given empty collection with no init"
        (is (= 0 (reduce + (decoding-reducible (open "") decode))))
        (is @closed?))
      (testing "given non-empty collection"
        (testing "with no init"
          (is (= 6 (reduce + (decoding-reducible (open "1 2 3") decode))))
          (is @closed?))
        (testing "with init"
          (is (= 10 (reduce + 4 (decoding-reducible (open "1 2 3") decode))))
          (is @closed?))
        (testing "with early termination"
          (is (= 1 (->> (decoding-reducible (open "1 2 3") decode)
                        (transduce (take 1) +))))
          (is @closed?))))))

(deftest t-csv-reducible
  (is (= [["1" "2" "3"]]
         (into [] (csv-reducible #(.getBytes "1,2,3"))))
      "should accept function")
  (is (= [["昨夜のコンサートは最高でした。"]]
         (into [] (csv-reducible (.getBytes "昨夜のコンサートは最高でした。" "SJIS")
                                 {:encoding "SJIS"})))
      "should respect encoding")
  (is (= [["1" "2,3"]]
         (into [] (csv-reducible (.getBytes "1,\"2,3\""))))
      "should default quote")
  (is (= [["1" "2" "3"]]
         (into [] (csv-reducible (.getBytes "1|2|3") {:separator \|})))
      "should respect separator")
  (is (= [["1" "2,3"]]
         (into [] (csv-reducible (.getBytes "1,|2,3|") {:quote \|})))
      "should respect quote")
  (is (= [{:one "4"}]
         (into [] (csv-reducible (.getBytes "1,2,3\n4,5,6")
                                 {:header {"1" :one}})))
      "should respect header")
  (is (= [["1" "2" "3"]]
         (into [] (csv-reducible #(.getBytes "\uFEFF1,2,3"))))
      "should not include BOM")
  (is (= [["1" "2" "3"]]
         (into [] (csv-reducible #(.getBytes "\uFEFF1,2,3")
                                 {:include-bom? false})))
      "should not include BOM")
  (is (= [["﻿1" "2" "3"]]
         (into [] (csv-reducible #(.getBytes "\uFEFF1,2,3")
                                 {:include-bom? true})))
      "should include BOM"))

(deftest t-edn-reducible
  (is (= [:foo]
         (into [] (edn-reducible #(.getBytes ":foo"))))
      "should accept function")
  (is (= [:昨夜のコンサートは最高でした。]
         (into [] (edn-reducible (.getBytes ":昨夜のコンサートは最高でした。" "SJIS")
                                 {:encoding "SJIS"})))
      "should respect encoding")
  (is (= [42]
         (into [] (edn-reducible (.getBytes "#foo \"bar\"")
                                 {:readers {'foo (fn [_] 42)}})))
      "should respect readers")
  (is (= [42]
         (into [] (edn-reducible (.getBytes "#foo \"bar\"")
                                 {:default (fn [_ _] 42)})))
      "should respect default"))

(deftest t-clojure-reducible
  (is (= [:foo]
         (into [] (clojure-reducible #(.getBytes ":foo"))))
      "should accept function")
  (is (= [:昨夜のコンサートは最高でした。]
         (into [] (clojure-reducible (.getBytes ":昨夜のコンサートは最高でした。"
                                                "SJIS")
                                     {:encoding "SJIS"})))
      "should respect encoding")
  (is (= [42]
         (into [] (clojure-reducible (.getBytes "#?(:foo 43
                                                 :clj 42)")
                                     {:read-cond :allow})))
      "should respect read-cond")
  (is (= [43]
         (into [] (clojure-reducible (.getBytes "#?(:foo 43
                                                 :clj 42)")
                                     {:read-cond :allow
                                      :features #{:foo}})))
      "should respect features")
  (is (= [42]
         (binding [*read-eval* true]
           (into [] (binding [*read-eval* false]
                      (clojure-reducible (.getBytes "#=(+ 40 2)"))))))
      "should inherit read-eval")
  (is (= [42]
         (binding [*read-eval* false]
           (into [] (clojure-reducible (.getBytes "#=(+ 40 2)")
                                       {:read-eval true}))))
      "should respect read-eval")
  (is (= [42]
         (binding [*data-readers* {'foo (fn [_] 42)}]
           (into [] (binding [*data-readers* nil]
                      (clojure-reducible (.getBytes "#foo \"bar\""))))))
      "should inherit data-readers")
  (is (= [42]
         (binding [*data-readers* nil]
           (into [] (clojure-reducible (.getBytes "#foo \"bar\"")
                                       {:data-readers {'foo (fn [_] 42)}}))))
      "should respect data-readers"))

(defrecord StrangeType [])

(deftest t-transit-reducible
  (let [write-handlers (transit/record-write-handlers StrangeType)
        read-handlers (transit/record-read-handlers StrangeType)
        default-handler (reify com.cognitect.transit.DefaultReadHandler
                          (fromRep [_ tag val]
                            42))]
    (letfn
        [(encode
           ([type obj]
            (encode type obj nil))
           ([type obj opts]
            (let [baos (ByteArrayOutputStream.)
                  writer (transit/writer baos type {:handlers write-handlers})]
              (transit/write writer obj)
              (.toByteArray baos))))]
      (is (= [:foo]
             (into [] (transit-reducible :json #(encode :json :foo))))
          "should accept function")
      (is (= [:foo]
             (into [] (transit-reducible :msgpack #(encode :msgpack :foo))))
          "should respect type")
      (let [obj (->StrangeType)]
        (is (= [obj]
               (into [] (transit-reducible :json
                                           (encode :json obj)
                                           {:handlers read-handlers})))
            "should respect handlers")
        (is (= [42]
               (into [] (transit-reducible :json
                                           (encode :json obj)
                                           {:default-handler default-handler})))
            "should respect default-handler")))))
