(ns reducibles.csv
  (:import
   (java.io PushbackReader)))

(defn parse-record
  [^PushbackReader reader quote ^long separator]
  (let [sb (StringBuilder.)]
    (letfn
        [(parse-field ^long [^long c]
           (if (= c quote)
             ;; parse quoted field
             (loop [c (.read reader)]
               (case c
                 ;; EOF means unterminated quoted field
                 -1 (throw (Exception. "Unterminated quoted field"))
                 (if (= c quote)
                   ;; quote could mean an escaped quote, or the end of the field
                   (let [c' (.read reader)]
                     (case c'
                       ;; EOL or EOF ...
                       (13 10 -1) c'
                       (cond
                         ;; or separator mean end of field
                         (= c' separator) c'
                         ;; escaped quote; append and continue
                         (= c' quote) (do (.append sb (char c'))
                                          (recur (.read reader))))))
                   ;; otherwise append and continue
                   (do (.append sb (char c))
                       (recur (.read reader))))))
             ;; parse unquoted field
             (loop [c c]
               (case c
                 ;; EOL or EOF ...
                 (13 10 -1) c
                 ;; or separator mean end of field
                 (if (= c separator)
                   c
                   ;; otherwise append and continue
                   (do (.append sb (char c))
                       (recur (.read reader))))))))]
      (let [c (.read reader)]
        (when (pos? c)
          (loop [record (transient [])
                 c c]
            (let [c (parse-field c)
                  record (conj! record (str sb))]
              (if (= c separator)
                (do (.setLength sb 0)
                    (recur record (.read reader)))
                (do (case (long c)
                      (10 -1) ::break
                      13 (let [c' (.read reader)]
                           (case c'
                             (10 -1) ::break
                             (.unread reader c'))))
                    (persistent! record))))))))))

(defn zip-headers
  [header]
  (fn zip-headers
    [rf]
    (let [headers (volatile! nil)]
      (fn transducer
        ([] (rf))
        ([result] (rf result))
        ([result input]
         (if-some [headers @headers]
           (rf result
               (persistent!
                (reduce (fn [m n]
                          (if-some [k (nth headers n)]
                            (assoc! m k (nth input n))
                            m))
                        (transient {})
                        (range (min (count headers) (count input))))))
           (do (vreset! headers (mapv header input))
               result)))))))
