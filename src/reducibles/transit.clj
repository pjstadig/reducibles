(ns reducibles.transit
  (:refer-clojure :exclude [read])
  (:require
   [clojure.java.io :as io]
   [cognitect.transit :as transit])
  (:import java.io.Closeable))

(defprotocol IRead
  (read [readable]))

(deftype CloseableTransitReader
    [^Closeable stream reader]
  IRead
  (read [_]
    (transit/read reader))
  Closeable
  (close [_]
    (.close stream)))

(defn reader
  [streamable type {:as opts :keys [handlers default-handler]}]
  (let [stream (io/input-stream streamable)
        reader (transit/reader stream
                               type
                               {:handlers handlers
                                :default-handler default-handler})]
    (->CloseableTransitReader stream reader)))
