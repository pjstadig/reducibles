(ns reducibles.core
  (:require
   [clojure.edn :as edn]
   [clojure.java.io :as io]
   [reducibles.csv :as csv])
  (:import
   (clojure.lang IReduce)
   (java.io Closeable PushbackReader)))

(defn decoding-reducible
  "Returns a reducible that opens a stream for decoding and ensures the stream is
  safely closed when reduction ends.  open should be a no argument function
  that returns a java.io.Closeable object.  decode should be a two argument
  function that takes the object return by open and an eof sentinel which
  should be returned if the end of the stream is reached.  decode will be
  called repeatedly until it returns the eof sentinel."
  ^clojure.lang.IReduce
  [open decode]
  (letfn
      [(reduce-loop [rf init stream]
         (loop [result init]
           (let [value (decode stream ::eof)]
             (if (= value ::eof)
               result
               (let [result' (rf result value)]
                 (if (reduced? result')
                   @result'
                   (recur result')))))))]
    (reify IReduce
      (reduce [_ rf]
        (with-open [stream ^Closeable (open)]
          (let [init (decode stream ::eof)]
            (if (= init ::eof)
              (unreduced (rf))
              (reduce-loop rf init stream)))))
      (reduce [_ rf init]
        (with-open [stream ^Closeable (open)]
          (reduce-loop rf init stream))))))

(defn binding-reducible
  "Returns a reducible that wraps an existing reducible and establishes bindings
  for the duration of reduction.  bindings-map is a binding maps as given to
  with-bindings.  reducible is the reducible that is being wrapped."
  ^clojure.lang.IReduce
  [binding-map ^IReduce reducible]
  (reify IReduce
    (reduce [_ rf]
      (with-bindings binding-map (.reduce reducible rf)))
    (reduce [_ rf init]
      (with-bindings binding-map (.reduce reducible rf init)))))

(defn buffered-reader
  (^java.io.BufferedReader
   [streamable]
   (buffered-reader streamable nil))
  (^java.io.BufferedReader
   [streamable encoding]
   (io/reader streamable :encoding encoding)))

(defn pushback-reader
  (^java.io.PushbackReader
   [streamable]
   (pushback-reader streamable nil))
  (^java.io.PushbackReader
   [streamable encoding]
   (if (instance? PushbackReader streamable)
     streamable
     (PushbackReader. (io/reader streamable :encoding encoding)))))

(defn csv-reducible
  "Returns a reducible that decodes delimited data.  readable should be an object
  that can be given to clojure.java.io/reader.  opts can contain the following
  options:

  :encoding -- a valid Java encoding, defaults to 'UTF-8'
  :quote -- a quote character used to quote string data, defaults to '\"'
  :separator -- a separator character used to separate fields, defaults to ','
  :header -- When specified rows are parsed into hashmaps using a header row.
             header should be a function taking a header as a single argument
             returning a value to use as that header's key in row data. If
             header returns nil, then that column is removed from every row that
             is parsed.  For example, to convert all headers to keywords use
             keyword as the header function, and to do something slightly more
             complex use a hashmap (e.g. {\"Price\" :price \"Model\" :model}).
             If header is not specified then all rows (including the first) are
             returned as vectors.
  :include-bom? -- if true, then include the Unicode Byte Order Mark at the
                   beginning of the file (if any). It will be prepended to the
                   first column.  You probably don't want this, which is why
                   this defaults to false."
  (^clojure.lang.IReduce
   [readable]
   (csv-reducible readable nil))
  (^clojure.lang.IReduce
   [readable {:as opts :keys [encoding quote separator header include-bom?]}]
   (let [quote (long (or quote \"))
         separator (long (or separator \,))]
     (letfn
         [(open []
            (let [reader (pushback-reader (if (fn? readable)
                                            (readable)
                                            readable)
                                          encoding)]
              (when-not include-bom?
                (let [c (.read reader)]
                  (when (and (not= 0xFEFF c) (not= -1 c))
                    (.unread reader c))))
              reader))
          (decode [reader eof]
            (or (csv/parse-record reader quote separator) eof))]
       (cond->> (decoding-reducible open decode)
         header (eduction (csv/zip-headers header)))))))

(defn edn-reducible
  "Returns a reducible that decodes EDN data.  readable should be an object that
  can be given to clojure.java.io/reader.  opts can contain the following
  options:

  :encoding -- a valid Java encoding, defaults to 'UTF-8'
  :readers -- a readers map passed aalong to clojure.edn/read
  :default -- a default reader passed along to clojure.edn.read"
  (^clojure.lang.IReduce
   [readable]
   (edn-reducible readable nil))
  (^clojure.lang.IReduce
   [readable {:as opts :keys [encoding readers default]}]
   (letfn
       [(open []
          (pushback-reader (if (fn? readable)
                             (readable)
                             readable)
                           encoding))
        (decode [reader eof]
          (edn/read {:eof eof
                     :readers readers
                     :default default}
                    reader))]
     (decoding-reducible open decode))))

(defn clojure-reducible
  "Returns a reducible that decodes Clojure data.  readable should be an object
  that can be given to clojure.java.io/reader.  opts can contain the following
  options:

  :encoding -- a valid Java encoding, defaults to 'UTF-8'
  :read-cond -- a value passed as the :read-cond option to clojure.core/read
  :features -- a value passed as the :features option to clojure.core/read
  :read-eval -- a value for *read-eval* established for the duration of a
                reduction.  If not specified, then the *read-eval* value
                established at the time and in the context of the
                reduction--*not* the context where the reducible is created--is
                used.
  :data-readers -- a value for *data-readers* established for the duration of a
                   reduction.  If not specified, then the *read-eval* value
                   established at the time and in the context of the
                   reduction--*not* the context where the reducible is
                   created--is used."
  (^clojure.lang.IReduce
   [readable]
   (clojure-reducible readable nil))
  (^clojure.lang.IReduce
   [readable {:as opts :keys [encoding read-cond features read-eval
                              data-readers]}]
   (letfn
       [(open []
          (pushback-reader (if (fn? readable)
                             (readable)
                             readable)
                           encoding))
        (decode [reader eof]
          (read {:eof eof
                 :read-cond read-cond
                 :features features}
                reader))]
     (let [binding-map (cond-> {}
                         (contains? opts :read-eval)
                         (assoc #'*read-eval* read-eval)
                         (contains? opts :data-readers)
                         (assoc #'*data-readers* data-readers))]
       (cond->> (decoding-reducible open decode)
         (seq binding-map) (binding-reducible binding-map))))))

(defn transit-reducible
  "Returns a reducible that decodes transit data.  readable should be an object
  that can be given to clojure.java.io/reader.  opts can contain the following
  options:

  :handlers -- a handlers map passed along to cognitect.transit/reader
  :default-handler -- a default handler passed along to cognitect.transit/reader"
  (^clojure.lang.IReduce
   [type streamable]
   (transit-reducible type streamable nil))
  (^clojure.lang.IReduce
   [type streamable {:as opts :keys [handlers default-handler]}]
   (require 'cognitect.transit)
   (require 'reducibles.transit)
   (let [reader @(ns-resolve 'reducibles.transit 'reader)
         read @(ns-resolve 'reducibles.transit 'read)]
     (letfn
         [(open []
            (reader (if (fn? streamable)
                      (streamable)
                      streamable)
                    type
                    {:handlers handlers
                     :default-handler default-handler}))
          (decode [stream eof]
            (try
              (read stream)
              (catch RuntimeException e
                (if (instance? java.io.EOFException (.getCause e))
                  eof
                  (throw e)))))]
       (decoding-reducible open decode)))))
